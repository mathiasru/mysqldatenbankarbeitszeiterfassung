# Semesterprojekt Datenbanken

## Einleitung
Dieses Abschlussprojekt beschäftigt sich mit dem Thema Datenbankmodellierung und Implementierung eines Zeiterfassungssystems für Mitarbeiter. Ziel ist es alle Schritte der Datenmodellierung möglichst praxisgerecht umzusetzen. Von der Anforderungsanalyse über das Entwerfen des Datenmodells, dem Mapping in ein relationales Modell bis hin zur Implementierung werden alle Ergebnisse in diesem Dokument zusammengefasst und beschrieben. Die Implementierung wird mittels Docker-Containern innerhalb einer virtuellen Umgebung, unter Verwendung von mysql 8.0.23 und adminer 4.8.1 umgesetzt.

<br>

# Inhaltsverzeichnis
- [Semesterprojekt Datenbanken](#semesterprojekt-datenbanken)
  - [Einleitung](#einleitung)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Verwendete Technologien](#verwendete-technologien)
- [Aufgabenstellung](#aufgabenstellung)
- [Vorbereitung](#vorbereitung)
  - [Vorbereitung des Servers](#vorbereitung-des-servers)
  - [Vorbereitung ssh-Remote Verbindung](#vorbereitung-ssh-remote-verbindung)
  - [Vorbereitung Datenbank mittels Docker-Containern](#vorbereitung-datenbank-mittels-docker-containern)
- [Durchführung](#durchführung)
- [Entity-Relationship Diagramm](#entity-relationship-diagramm)
- [Relationales Datenbankmodell](#relationales-datenbankmodell)
- [Physisches Datenbankmodell](#physisches-datenbankmodell)
  - [Statements zur Datenbank Erstellung](#statements-zur-datenbank-erstellung)
  - [Aufbau der Datenbank](#aufbau-der-datenbank)
  - [Statements für zentrale Interaktionen mit Datenbank](#statements-für-zentrale-interaktionen-mit-datenbank)
    - [INSERT-Statements](#insert-statements)
    - [SELECT-Statements](#select-statements)
    - [Ausgabe SELECT-Statement](#ausgabe-select-statement)
- [Diskussion](#diskussion)
- [Literaturverzeichnis](#literaturverzeichnis)
  - [Author](#author)
