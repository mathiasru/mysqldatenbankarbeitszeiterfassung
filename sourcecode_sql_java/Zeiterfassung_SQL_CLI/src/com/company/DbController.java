package com.company;

import java.sql.*;

public class DbController {

    //Anpassen der Verbindung zur MySQL Datenbank
    //public static final String URL = "jdbc:mysql://192.168.56.30:3306/zeiterfassung";
    public static final String URL = "jdbc:mysql://192.168.56.31:3306/kleinprojekt_test";
    public static final String USER = "root";
    public static final String PASSWORD = "123";
    public static Connection conn;
    //Ausgabe Spaltenbreiten
    public static final int KURZ = 3;
    public static final int MITTEL = 15;
    public static final int LANG = 30;

    public static boolean connect() {
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Verbindung erfolgreich!");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String strichLaenge(int laenge) {
        String strich = "";
        for (int i = 0; i < laenge; i++) {
            strich += "-";
        }
        return strich;
    }

    public static void selectMitarbeiter() {
        try {

            //SQL Statement
            String query = "SELECT\n" +
                    "       mitarbeiter.mitarbeiter_id AS 'ID',\n" +
                    "       mitarbeiter.vorname AS 'Vorname',\n" +
                    "       mitarbeiter.nachname AS 'Nachname',\n" +
                    "       mitarbeiter.geburtsdatum AS 'Geburtsdatum',\n" +
                    "       mitarbeiter.einstelldatum AS 'Einstelldatum',\n" +
                    "       CONCAT(adresse.strasse, ' ', adresse.hausnummer) AS 'Strasse',\n" +
                    "       adresse.plz AS 'PLZ',\n" +
                    "       adresse.ort AS 'Ort'\n" +
                    "FROM \n" +
                    "      mitarbeiter\n" +
                    "INNER JOIN adresse USING(adresse_id)\n" +
                    "ORDER BY mitarbeiter_id ASC;";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            //Spalten
            int columns = rs.getMetaData().getColumnCount();
            int laenge = 0;
            for (int i = 1; i <= columns; i++) {
                if (i == 1) {
                    System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.KURZ;
                } else if (i == 6) {
                    System.out.print(String.format("%-" + DbController.LANG + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.LANG;
                } else {
                    System.out.print(String.format("%-" + DbController.MITTEL + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.MITTEL;
                }

            }

            //Strich
            System.out.println();
            System.out.println(strichLaenge(laenge));
            System.out.println();

            //Zeilen
            while (rs.next()) {
                for (int i = 1; i <= columns; i++) {
                    if (i == 1) {
                        System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getString(i)));
                    } else if (i == 6) {
                        System.out.print(String.format("%-" + DbController.LANG + "s", rs.getString(i)));
                    } else {
                        System.out.print(String.format("%-" + DbController.MITTEL + "s", rs.getString(i)));
                    }
                }
                System.out.println();
            }

            rs.close();
            stmt.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void tageHomeofficeAktuellesJahr() {
        try {

            //SQL Statement
            String query = "SELECT  mitarbeiter.mitarbeiter_id AS 'ID', \n" +
                    "        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', \n" +
                    "        ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(arbeitszeit_bis, arbeitszeit_von) < 0 \n" +
                    "        THEN (SUBTIME(arbeitszeit_von, arbeitszeit_bis))\n" +
                    "        ELSE SUBTIME(arbeitszeit_bis, arbeitszeit_von) END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'TageHomeofficeAktuellesJahr'\n" +
                    "FROM anwesenheit\n" +
                    "    INNER JOIN mitarbeiter ON anwesenheit.mitarbeiter_id = mitarbeiter.mitarbeiter_id\n" +
                    "    INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id\n" +
                    "WHERE anwesenheit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)\n" +
                    "      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) \n" +
                    "      AND anwesenheit.anwesenheits_art = 'Homeoffice' \n" +
                    "GROUP BY mitarbeiter.mitarbeiter_id\n" +
                    "ORDER BY mitarbeiter.mitarbeiter_id ASC;";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            //Spalten
            int columns = rs.getMetaData().getColumnCount();
            int laenge = 0;
            for (int i = 1; i <= columns; i++) {
                if (i == 1) {
                    System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.KURZ;
                } else {
                    System.out.print(String.format("%-" + DbController.LANG + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.LANG;
                }
            }

            //Strich
            System.out.println();
            System.out.println(strichLaenge(laenge));
            System.out.println();

            //Zeilen
            while (rs.next()) {
                for (int i = 1; i <= columns; i++) {
                    if (i == 1) {
                        System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getString(i)));
                    } else {
                        System.out.print(String.format("%-" + DbController.LANG + "s", rs.getString(i)));
                    }
                }
                System.out.println();
            }

            rs.close();
            stmt.close();
        } catch (
                SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void krankenstand() {
        try {

            //SQL Statement
            String query = "SELECT  mitarbeiter.mitarbeiter_id AS 'ID',\n" +
                    "        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', \n" +
                    "        ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(fehlzeit_bis, fehlzeit_von) < 0 THEN (SUBTIME(fehlzeit_von, fehlzeit_bis))\n" +
                    "        ELSE SUBTIME(fehlzeit_bis, fehlzeit_von) \n" +
                    "        END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'TageKrankenstandAktuellesJahr' \n" +
                    "FROM fehlzeit\n" +
                    "INNER JOIN mitarbeiter ON fehlzeit.mitarbeiter_id = mitarbeiter.mitarbeiter_id\n" +
                    "INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id\n" +
                    "WHERE fehlzeit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)\n" +
                    "      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) \n" +
                    "      AND fehlzeit.grund = 'Krank'\n" +
                    "GROUP BY mitarbeiter.mitarbeiter_id\n" +
                    "ORDER BY mitarbeiter.mitarbeiter_id ASC;";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            //Spalten
            int columns = rs.getMetaData().getColumnCount();
            int laenge = 0;
            for (int i = 1; i <= columns; i++) {
                if (i == 1) {
                    System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.KURZ;
                } else {
                    System.out.print(String.format("%-" + DbController.LANG + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.LANG;
                }
            }

            //Strich
            System.out.println();
            System.out.println(strichLaenge(laenge));
            System.out.println();

            //Zeilen
            while (rs.next()) {
                for (int i = 1; i <= columns; i++) {
                    if (i == 1) {
                        System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getString(i)));
                    } else {
                        System.out.print(String.format("%-" + DbController.LANG + "s", rs.getString(i)));
                    }
                }
                System.out.println();
            }

            rs.close();
            stmt.close();
        } catch (
                SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void unentschuldigteFehlstunden() {
        try {

            //SQL Statement
            String query = "SELECT  mitarbeiter.mitarbeiter_id AS 'ID',\n" +
                    "        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', \n" +
                    "        COUNT(fehlzeit.entschuldigt) AS 'UnentschuldigteFehlzeiten' \n" +
                    "FROM fehlzeit\n" +
                    "INNER JOIN mitarbeiter ON fehlzeit.mitarbeiter_id = mitarbeiter.mitarbeiter_id\n" +
                    "INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id\n" +
                    "WHERE fehlzeit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)\n" +
                    "      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) \n" +
                    "      AND fehlzeit.entschuldigt = 0\n" +
                    "GROUP BY mitarbeiter.mitarbeiter_id\n" +
                    "ORDER BY mitarbeiter.mitarbeiter_id ASC;";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            //Spalten
            int columns = rs.getMetaData().getColumnCount();
            int laenge = 0;
            for (int i = 1; i <= columns; i++) {
                if (i == 1) {
                    System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.KURZ;
                } else {
                    System.out.print(String.format("%-" + DbController.LANG + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.LANG;
                }
            }

            //Strich
            System.out.println();
            System.out.println(strichLaenge(laenge));
            System.out.println();

            //Zeilen
            while (rs.next()) {
                for (int i = 1; i <= columns; i++) {
                    if (i == 1) {
                        System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getString(i)));
                    } else {
                        System.out.print(String.format("%-" + DbController.LANG + "s", rs.getString(i)));
                    }
                }
                System.out.println();
            }

            rs.close();
            stmt.close();
        } catch (
                SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void arbeitszeitHomeoffice() {
        try {

            //SQL Statement
            String query = "Select  mitarbeiter.mitarbeiter_id AS 'ID', \n" +
                    "CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', \n" +
                    "anwesenheit.datum AS 'Datum',\n" +
                    "ROUND(TIME_TO_SEC(CASE WHEN SUBTIME(arbeitszeit_bis, arbeitszeit_von) < 0 THEN (SUBTIME(arbeitszeit_von, arbeitszeit_bis))\n" +
                    "        ELSE SUBTIME(arbeitszeit_bis, arbeitszeit_von) END)/3600, 2) AS 'ArbeitszeitHomeoffice'\n" +
                    "From anwesenheit\n" +
                    "INNER JOIN mitarbeiter ON anwesenheit.mitarbeiter_id = mitarbeiter.mitarbeiter_id\n" +
                    "INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id\n" +
                    "WHERE anwesenheit.datum >= DATE_FORMAT(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH), '%Y-%m-01') \n" +
                    "      AND anwesenheit.datum <= LAST_DAY(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH))\n" +
                    "      AND anwesenheit.anwesenheits_art = 'Homeoffice'\n" +
                    "ORDER BY mitarbeiter.mitarbeiter_id, anwesenheit.datum ASC;";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            //Spalten
            int columns = rs.getMetaData().getColumnCount();
            int laenge = 0;
            for (int i = 1; i <= columns; i++) {
                if (i == 1) {
                    System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.KURZ;
                } else if (i == 3) {
                    System.out.print(String.format("%-" + DbController.MITTEL + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.MITTEL;
                } else {
                    System.out.print(String.format("%-" + DbController.LANG + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.LANG;
                }
            }

            //Strich
            System.out.println();
            System.out.println(strichLaenge(laenge));
            System.out.println();

            //Zeilen
            while (rs.next()) {
                for (int i = 1; i <= columns; i++) {
                    if (i == 1) {
                        System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getString(i)));
                    } else if (i == 3) {
                        System.out.print(String.format("%-" + DbController.MITTEL + "s", rs.getString(i)));
                    } else {
                        System.out.print(String.format("%-" + DbController.LANG + "s", rs.getString(i)));
                    }
                }
                System.out.println();
            }

            rs.close();
            stmt.close();
        } catch (
                SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void arbeitszeitGesamt() {
        try {

            //SQL Statement
            String query = "Select  mitarbeiter.mitarbeiter_id AS 'ID', CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', \n" +
                    "ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(arbeitszeit_bis, arbeitszeit_von) < 0 THEN (SUBTIME(arbeitszeit_von, arbeitszeit_bis))\n" +
                    "        ELSE SUBTIME(arbeitszeit_bis, arbeitszeit_von) END))/3600, 2) AS 'IstStundenVormonat',\n" +
                    "        ROUND(DAYOFMONTH(LAST_DAY(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH)))/7*sollarbeitszeit.wochenstunden, 2) AS 'SollStunden'\n" +
                    "From anwesenheit\n" +
                    "INNER JOIN mitarbeiter ON anwesenheit.mitarbeiter_id = mitarbeiter.mitarbeiter_id\n" +
                    "INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id\n" +
                    "WHERE anwesenheit.datum >= DATE_FORMAT(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH), '%Y-%m-01') \n" +
                    "      AND anwesenheit.datum <= LAST_DAY(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH))\n" +
                    "GROUP BY mitarbeiter.mitarbeiter_id\n" +
                    "ORDER BY mitarbeiter.mitarbeiter_id ASC;";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            //Spalten
            int columns = rs.getMetaData().getColumnCount();
            int laenge = 0;
            for (int i = 1; i <= columns; i++) {
                if (i == 1) {
                    System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.KURZ;
                } else {
                    System.out.print(String.format("%-" + DbController.LANG + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.LANG;
                }
            }

            //Strich
            System.out.println();
            System.out.println(strichLaenge(laenge));
            System.out.println();

            //Zeilen
            while (rs.next()) {
                for (int i = 1; i <= columns; i++) {
                    if (i == 1) {
                        System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getString(i)));
                    } else {
                        System.out.print(String.format("%-" + DbController.LANG + "s", rs.getString(i)));
                    }
                }
                System.out.println();
            }

            rs.close();
            stmt.close();
        } catch (
                SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void urlaubInformation() {
        try {

            //SQL Statement
            String query = "SELECT  mitarbeiter.mitarbeiter_id AS 'ID',\n" +
                    "        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', \n" +
                    "        ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(fehlzeit_bis, fehlzeit_von) < 0 \n" +
                    "        THEN (SUBTIME(fehlzeit_von, fehlzeit_bis)) \n" +
                    "        ELSE SUBTIME(fehlzeit_bis, fehlzeit_von) \n" +
                    "        END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'VerbrauchteUrlaubsTage',\n" +
                    "        urlaub.urlaubsanspruch_pro_jahr - ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(fehlzeit_bis, fehlzeit_von) < 0 \n" +
                    "        THEN (SUBTIME(fehlzeit_von, fehlzeit_bis))\n" +
                    "        ELSE SUBTIME(fehlzeit_bis, fehlzeit_von) \n" +
                    "        END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'VerfuegbarerUrlaub'\n" +
                    "FROM fehlzeit\n" +
                    "INNER JOIN mitarbeiter ON fehlzeit.mitarbeiter_id = mitarbeiter.mitarbeiter_id\n" +
                    "INNER JOIN urlaub ON mitarbeiter.urlaub_id = urlaub.urlaub_id\n" +
                    "INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id\n" +
                    "WHERE fehlzeit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)\n" +
                    "      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) \n" +
                    "      AND fehlzeit.grund = 'Urlaub'\n" +
                    "GROUP BY mitarbeiter.mitarbeiter_id\n" +
                    "ORDER BY mitarbeiter.mitarbeiter_id ASC;";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            //Spalten
            int columns = rs.getMetaData().getColumnCount();
            int laenge = 0;
            for (int i = 1; i <= columns; i++) {
                if (i == 1) {
                    System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.KURZ;
                } else {
                    System.out.print(String.format("%-" + DbController.LANG + "s", rs.getMetaData().getColumnLabel(i)));
                    laenge += DbController.LANG;
                }
            }

            //Strich
            System.out.println();
            System.out.println(strichLaenge(laenge));
            System.out.println();

            //Zeilen
            while (rs.next()) {
                for (int i = 1; i <= columns; i++) {
                    if (i == 1) {
                        System.out.print(String.format("%-" + DbController.KURZ + "s", rs.getString(i)));
                    } else {
                        System.out.print(String.format("%-" + DbController.LANG + "s", rs.getString(i)));
                    }
                }
                System.out.println();
            }

            rs.close();
            stmt.close();
        } catch (
                SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
