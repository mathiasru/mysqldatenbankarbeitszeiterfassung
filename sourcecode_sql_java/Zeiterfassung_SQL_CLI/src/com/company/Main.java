package com.company;

public class Main {

    public static void main(String[] args) {
        if (DbController.connect()) {
            Kommandozeilenmenu.start();
        } else {
            System.out.println("Verbindung NICHT erfolgreich!");
        }

    }
}
