use zeiterfassung;

-- Das folgende SELECT-STATEMENT dient zur Ermittlung aller Mitarbeiter un deren Adresse,
-- zudem wird die Ausgabe anhand der mitarbeiter_id aufsteigend sortiert.
SELECT
       mitarbeiter.mitarbeiter_id AS 'ID',
       mitarbeiter.vorname AS 'Vorname',
       mitarbeiter.nachname AS 'Nachname',
       mitarbeiter.geburtsdatum AS 'Geburtsdatum',
       mitarbeiter.einstelldatum AS 'Einstelldatum',
       CONCAT(adresse.strasse, ' ', adresse.hausnummer) AS 'Strasse',
       adresse.plz AS 'PLZ',
       adresse.ort AS 'Ort'
FROM 
      mitarbeiter
INNER JOIN adresse USING(adresse_id)
ORDER BY mitarbeiter_id ASC;


SET @anwesenheits_art = 'Homeoffice';
-- SET @anwesenheits_art = 'Buero';
-- SET @anwesenheits_art = 'Montage';

-- Das folgende SELECT-STATEMENT dient zur Ermittlung der Gesamt-Arbeitstage des aktuellen Jahres pro Mitarbeiter anhand einer bestimmten Art, 
-- somit kann der Anteil der jeweiligen Art ermittelt werden. Zudem wird die Ausgabe anhand der mitarbeiter_id aufsteigend sortiert.
Select  mitarbeiter.mitarbeiter_id AS 'ID', 
        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
        -- Berechnen der verbrauchten Urlaubstage innerhalb eines Jahres, gerundet auf zwei Nachkommastellen. 
        -- Für die Berechnung muss die Zeit zuerst in Sekunden umgewandelt werden um den Wert richtig ermitteln zu können, 
        -- zudem wird in einer "Case When-Bedingung" sichergestellt, dass nur positive Zahlen ermittelt werden.
        ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(arbeitszeit_bis, arbeitszeit_von) < 0 
        THEN (SUBTIME(arbeitszeit_von, arbeitszeit_bis))
        ELSE SUBTIME(arbeitszeit_bis, arbeitszeit_von) END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'TageHomeofficeAktuellesJahr'
From anwesenheit
    INNER JOIN mitarbeiter ON anwesenheit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
    INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
-- Hier wird vom aktuellen Jahr der erste Tag zu ermittelt.
WHERE anwesenheit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)
      -- Hier wird vom aktuellen Jahr der ersteletzte Tag ermittelt.
      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) 
      AND anwesenheit.anwesenheits_art = @anwesenheits_art -- Variable von oben wird verwendet
GROUP BY mitarbeiter.mitarbeiter_id
ORDER BY mitarbeiter.mitarbeiter_id ASC;


-- Das folgende SELECT-STATEMENT dient zur Ermittlung der Arbeitszeitseinträge des Vormonats pro Mitarbeiter
-- anhand einer bestimmten Art, welche anhand der mitarbeiter_id und dem Datumsstempel aufsteigend sortiert werden.
Select  mitarbeiter.mitarbeiter_id AS 'ID', 
CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
anwesenheit.datum AS 'Datum',
ROUND(TIME_TO_SEC(CASE WHEN SUBTIME(arbeitszeit_bis, arbeitszeit_von) < 0 THEN (SUBTIME(arbeitszeit_von, arbeitszeit_bis))
        ELSE SUBTIME(arbeitszeit_bis, arbeitszeit_von) END)/3600, 2) AS 'ArbeitszeitHomeoffice'
From anwesenheit
INNER JOIN mitarbeiter ON anwesenheit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
WHERE anwesenheit.datum >= DATE_FORMAT(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH), '%Y-%m-01') 
      AND anwesenheit.datum <= LAST_DAY(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH))
      AND anwesenheit.anwesenheits_art = @anwesenheits_art
ORDER BY mitarbeiter.mitarbeiter_id, anwesenheit.datum ASC;


-- Das folgende SELECT-STATEMENT dient zur Ermittlung der Gesamt-Arbeitszeit des Vormonats pro Mitarbeiter, 
-- welche anhand der mitarbeiter_id aufsteigend sortiert wird.
Select  mitarbeiter.mitarbeiter_id AS 'ID', CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(arbeitszeit_bis, arbeitszeit_von) < 0 THEN (SUBTIME(arbeitszeit_von, arbeitszeit_bis))
        ELSE SUBTIME(arbeitszeit_bis, arbeitszeit_von) END))/3600, 2) AS 'IstStundenVormonat',
        ROUND(DAYOFMONTH(LAST_DAY(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH)))/7*sollarbeitszeit.wochenstunden, 2) AS 'SollStunden'
From anwesenheit
INNER JOIN mitarbeiter ON anwesenheit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
WHERE anwesenheit.datum >= DATE_FORMAT(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH), '%Y-%m-01') 
      AND anwesenheit.datum <= LAST_DAY(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH))
GROUP BY mitarbeiter.mitarbeiter_id
ORDER BY mitarbeiter.mitarbeiter_id ASC;


-- Das folgende SELECT-STATEMENT dient zur Ermittlung der Krankenstandtage des aktuellen Jahres pro Mitarbeiter,
-- welche anhand der mitarbeiter_id aufsteigend sortiert wird.
Select  mitarbeiter.mitarbeiter_id AS 'ID',
        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
        ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(fehlzeit_bis, fehlzeit_von) < 0 THEN (SUBTIME(fehlzeit_von, fehlzeit_bis))
        ELSE SUBTIME(fehlzeit_bis, fehlzeit_von) 
        END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'TageKrankenstandAktuellesJahr' 
From fehlzeit
INNER JOIN mitarbeiter ON fehlzeit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
WHERE fehlzeit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)
      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) 
      AND fehlzeit.grund = 'Krank'
GROUP BY mitarbeiter.mitarbeiter_id
ORDER BY mitarbeiter.mitarbeiter_id ASC;


-- Das folgende SELECT-STATEMENT dient zur Ermittlung der unentschuldigten Fehlzeiten des aktuellen Jahres pro Mitarbeiter,
-- welche anhand der mitarbeiter_id aufsteigend sortiert wird.
Select  mitarbeiter.mitarbeiter_id AS 'ID',
        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
        COUNT(fehlzeit.entschuldigt) AS 'UnentschuldigteFehlzeitenAktullesJahr' 
From fehlzeit
INNER JOIN mitarbeiter ON fehlzeit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
WHERE fehlzeit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)
      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) 
      AND fehlzeit.entschuldigt = 0
GROUP BY mitarbeiter.mitarbeiter_id
ORDER BY mitarbeiter.mitarbeiter_id ASC;


-- Das folgende SELECT-STATEMENT dient zur Ermittlung der schon verbrauchten Urlaubstage und der noch verfügbaren anhand des
-- aktuellen Jahres unter Berücksichtigung der Gesamtanzahl an Urlaubstagen pro Jahr. Zudem wird die Ausgabe anhand der 
-- mitarbeiter_id aufsteigend sortiert.
Select  mitarbeiter.mitarbeiter_id AS 'ID',
        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
        ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(fehlzeit_bis, fehlzeit_von) < 0 
        THEN (SUBTIME(fehlzeit_von, fehlzeit_bis)) 
        ELSE SUBTIME(fehlzeit_bis, fehlzeit_von) 
        END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'VerbrauchteUrlaubsTageAktuellesJahr',
        -- Selbes Scheme als in der voherigen Berechnung, nur dass der ermittelte Wert der Gesamtanzahl der Urlaubstage abgezogen wird.
        urlaub.urlaubsanspruch_pro_jahr - ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(fehlzeit_bis, fehlzeit_von) < 0 
        THEN (SUBTIME(fehlzeit_von, fehlzeit_bis))
        ELSE SUBTIME(fehlzeit_bis, fehlzeit_von) 
        END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'VerfuegbarerUrlaub'
From fehlzeit
INNER JOIN mitarbeiter ON fehlzeit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
INNER JOIN urlaub ON mitarbeiter.urlaub_id = urlaub.urlaub_id
INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
WHERE fehlzeit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)
      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) 
      AND fehlzeit.grund = 'Urlaub'
GROUP BY mitarbeiter.mitarbeiter_id
ORDER BY mitarbeiter.mitarbeiter_id ASC;