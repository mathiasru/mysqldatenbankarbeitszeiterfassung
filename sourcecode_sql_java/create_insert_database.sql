-- verwenden einer bereits erstellten Datenbank
use zeiterfassung;

-- Wichtig, die Reihenfolge muss beim Löschen und Erstellen von Tabellen beachtet werden (Foreign-Key Constraint)

-- Löschen der Tabellen falls vorhanden
DROP TABLE IF EXISTS fehlzeit;
DROP TABLE IF EXISTS anwesenheit;
DROP TABLE IF EXISTS mitarbeiter;
DROP TABLE IF EXISTS adresse;
DROP TABLE IF EXISTS sollarbeitszeit;
DROP TABLE IF EXISTS urlaub;


-- Erstellen der Tabellen
CREATE TABLE IF NOT EXISTS adresse (
    adresse_id int NOT NULL auto_increment PRIMARY KEY,
    strasse varchar(255) NOT NULL,
    hausnummer varchar(255) NOT NULL,
    plz char(4) NOT NULL,
    ort varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS sollarbeitszeit (
    sollarbeitszeit_id int NOT NULL auto_increment PRIMARY KEY,
    wochenstunden float NOT NULL,
    stunden_pro_tag float NOT NULL
);

CREATE TABLE IF NOT EXISTS urlaub (
    urlaub_id int NOT NULL auto_increment PRIMARY KEY,
    urlaubsanspruch_pro_jahr float NOT NULL
);

CREATE TABLE IF NOT EXISTS mitarbeiter (
    mitarbeiter_id int NOT NULL auto_increment PRIMARY KEY,
    vorname varchar(255) NOT NULL,
    nachname varchar(255) NOT NULL,
    geburtsdatum date NOT NULL,
    einstelldatum date NOT NULL,
    adresse_id int NOT NULL,
    sollarbeitszeit_id int NOT NULL,
    urlaub_id int NOT NULL,
    foreign key(adresse_id) references adresse (adresse_id),
    foreign key(sollarbeitszeit_id) references sollarbeitszeit (sollarbeitszeit_id),
    foreign key(urlaub_id) references urlaub (urlaub_id)
);

CREATE TABLE IF NOT EXISTS anwesenheit (
    anwesenheit_id int NOT NULL auto_increment PRIMARY KEY,
    datum date NOT NULL,
    arbeitszeit_von time NOT NULL,
    arbeitszeit_bis time NOT NULL,
    anwesenheits_art varchar(255) NULL,
    mitarbeiter_id int NOT NULL,
    foreign key(mitarbeiter_id) references mitarbeiter (mitarbeiter_id)
);

CREATE TABLE IF NOT EXISTS fehlzeit (
    fehlzeit_id int NOT NULL auto_increment PRIMARY KEY,
    datum date NOT NULL,
    fehlzeit_von time NOT NULL,
    fehlzeit_bis time NOT NULL,
    grund varchar(255) NOT NULL,
    entschuldigt boolean NOT NULL,
    mitarbeiter_id int NOT NULL,
    foreign key(mitarbeiter_id) references mitarbeiter (mitarbeiter_id)
);

-- Erstellen vno Datensätzen 

-- ADRESSE
INSERT INTO `adresse` (`adresse_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES (1, 'Schnann', '4', '6574', 'Schnann');
INSERT INTO `adresse` (`adresse_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES (2, 'Prantauersiedlung', '21', '6500', 'Landeck');
INSERT INTO `adresse` (`adresse_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES (3, 'Via-Claudia-Augusta', '29', '6533', 'Fiss');
INSERT INTO `adresse` (`adresse_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES (4, 'Eileweg', '6', '6522', 'Prutz');
INSERT INTO `adresse` (`adresse_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES (5, 'Hauptstrasse', '94', '6511', 'Zams');

-- SOLLARBEITSZEIT
INSERT INTO `sollarbeitszeit` (`sollarbeitszeit_id`, `wochenstunden`, `stunden_pro_tag`) VALUES (1, 40.0, 8);
INSERT INTO `sollarbeitszeit` (`sollarbeitszeit_id`, `wochenstunden`, `stunden_pro_tag`) VALUES (2, 38.5, 7.7);

-- URLAUB
INSERT INTO `urlaub` (`urlaub_id`, `urlaubsanspruch_pro_jahr`) VALUES (1, 25.0);

-- MITARBEITER
INSERT INTO `mitarbeiter` (`mitarbeiter_id`, `vorname`, `nachname`, `geburtsdatum`, `einstelldatum`, `adresse_id`, `sollarbeitszeit_id`, `urlaub_id`) VALUES (1, 'Clemens', 'Kerber', '1999-06-22', '2021-01-01', 1, 2, 1);
INSERT INTO `mitarbeiter` (`mitarbeiter_id`, `vorname`, `nachname`, `geburtsdatum`, `einstelldatum`, `adresse_id`, `sollarbeitszeit_id`, `urlaub_id`) VALUES (2, 'Tobias', 'Tilg', '2002-02-20', '2020-01-01', 2, 1, 1);
INSERT INTO `mitarbeiter` (`mitarbeiter_id`, `vorname`, `nachname`, `geburtsdatum`, `einstelldatum`, `adresse_id`, `sollarbeitszeit_id`, `urlaub_id`) VALUES (3, 'Frederick', 'Moederndorfer', '1998-03-13', '2021-01-15', 3, 2, 1);
INSERT INTO `mitarbeiter` (`mitarbeiter_id`, `vorname`, `nachname`, `geburtsdatum`, `einstelldatum`, `adresse_id`, `sollarbeitszeit_id`, `urlaub_id`) VALUES (4, 'Dario', 'Skocibusic', '1993-06-23', '2020-05-01', 4, 1, 1);
INSERT INTO `mitarbeiter` (`mitarbeiter_id`, `vorname`, `nachname`, `geburtsdatum`, `einstelldatum`, `adresse_id`, `sollarbeitszeit_id`, `urlaub_id`) VALUES (5, 'Mathias', 'Rudig', '1993-01-29', '2020-05-01', 5, 1, 1);

-- ANWESENHEIT
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (1, '2021-05-24', '04:24:42', '20:01:09', 'Buero', 1);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (2, '2021-05-25', '04:50:38', '06:07:48', 'Homeoffice', 1);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (3, '2021-05-26', '18:05:17', '03:24:45', 'Homeoffice', 1);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (4, '2021-05-27', '04:22:38', '08:32:59', 'Homeoffice', 1);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (5, '2021-05-28', '14:24:58', '21:43:01', 'Homeoffice', 1);

INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (6, '2021-05-24', '04:24:42', '20:01:09', 'Buero', 2);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (7, '2021-05-25', '04:50:38', '06:07:48', 'Buero', 2);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (8, '2021-05-26', '18:05:17', '03:24:45', 'Buero', 2);

INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (9, '2021-05-24', '04:24:42', '20:01:09', 'Buero', 3);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (10, '2021-05-25', '04:50:38', '06:07:48', 'Aussendienst', 3);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (11, '2021-05-26', '18:05:17', '03:24:45', 'Aussendienst', 3);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (12, '2021-05-27', '04:22:38', '08:32:59', 'Aussendienst', 3);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (13, '2021-05-28', '14:24:58', '21:43:01', 'Aussendienst', 3);

INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (14, '2021-05-24', '04:24:42', '20:01:09', 'Buero', 4);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (15, '2021-05-25', '04:50:38', '06:07:48', 'Buero', 4);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (16, '2021-05-26', '18:05:17', '03:24:45', 'Buero', 4);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (17, '2021-05-27', '04:22:38', '08:32:59', 'Buero', 4);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (18, '2021-05-28', '14:24:58', '21:43:01', 'Homeoffice', 4);

INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (19, '2021-05-24', '04:24:42', '20:01:09', 'Buero', 5);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (20, '2021-05-25', '04:50:38', '06:07:48', 'Buero', 5);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (21, '2021-05-26', '18:05:17', '03:24:45', 'Buero', 5);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (22, '2021-05-27', '04:22:38', '08:32:59', 'Buero', 5);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (23, '2021-05-28', '14:24:58', '21:43:01', 'Buero', 5);
-- FEHLZEIT
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (1, '2021-05-31', '07:30:00', '15:12:00', 'Urlaub', 1, 1);

INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (2, '2021-05-27', '07:30:00', '15:30:00', 'Krank', 0, 2);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (3, '2021-05-08', '07:30:00', '15:30:00', 'Krank', 1, 2);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (4, '2021-05-31', '07:30:00', '15:30:00', 'Urlaub', 1, 2);

INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (5, '2021-05-17', '07:30:00', '15:12:00', 'Krank', 0, 3);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (6, '2021-05-18', '07:30:00', '15:12:00', 'Krank', 0, 3);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (7, '2021-05-19', '07:30:00', '15:12:00', 'Krank', 0, 3);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (8, '2021-05-20', '07:30:00', '15:12:00', 'Krank', 0, 3);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (9, '2021-05-21', '07:30:00', '15:12:00', 'Krank', 0, 3);

INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (10, '2021-05-17', '07:30:00', '15:30:00', 'Krank', 1, 4);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (11, '2021-05-18', '07:30:00', '15:30:00', 'Urlaub', 1, 4);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (12, '2021-05-19', '07:30:00', '15:30:00', 'Urlaub', 1, 4);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (13, '2021-05-20', '07:30:00', '15:30:00', 'Urlaub', 1, 4);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (14, '2021-05-21', '07:30:00', '15:30:00', 'Urlaub', 1, 4);

INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (15, '2021-05-31', '07:30:00', '15:30:00', 'Urlaub', 1, 5);
